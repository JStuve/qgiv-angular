# QgivAngular

This project is to demonstrate how to add QGiv form widget onto an Angular page. We are using Bootstrap navigate and Angular routing to switch between different views.

## Run App

1. Clone Repo
2. In terminal change directory to project
3. npm install
4. ng serve --open


## Issue

Whenever you go between different navigations, the QGiv form does not load. For testing purposes, you will know the form is loaded if you see the form up to "What type of donation are you making?"
